document.getElementById('enviar').addEventListener('click', function() {
    let nombre = document.getElementById("nombre").value,
        apellido = document.getElementById('apellido').value,
        edad = parseInt(document.getElementById('edad').value),
        licencia = document.getElementById('licencia').value,
        fechaExp = parseInt(document.getElementById('exp').value),

        fechaActual = 20210708;

    if (nombre === "" || apellido === "" || edad === "" || fechaExp === "") {
        console.log("Todos los campos son obligatorios");
    } else {

        if (edad >= 18 && fechaExp > fechaActual) {
            console.log(`${nombre} ${apellido} es apto para conducir`);
        } else {
            console.log(`${nombre} ${apellido} NO es apto para conducir`);
        }
    }
})
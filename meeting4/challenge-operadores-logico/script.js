let cond1 = (1 == '1'),
    cond2 = (1 === '1'),
    cond3 = (0 || 1),
    cond4 = (0 && 1),
    cond5 = (true && 1),
    cond6 = (false && 1),
    cond7 = (true || 1),
    cond8 = (false || 1),
    cond9 = (1 - 1) && (2 + 2),
    cond10 = (1 - 1) || (2 + 2);

//salidas por consola
console.log(cond1); //boolean
console.log(cond2); //boolean
console.log(cond3); //number
console.log(cond4); //number
console.log(cond5); //number
console.log(cond6); //boolean
console.log(cond7); //boolean
console.log(cond8); //number
console.log(cond9); //number
console.log(cond10); //number
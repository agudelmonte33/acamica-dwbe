const express = require('express');
const app = express();
const config = require('./config');
const Account = require('./models/account');
const mongoose = require('mongoose')

app.use(express.json());

app.post('/accounts', async (req, res) => {
    try {
        const {
            first_name,
            last_name,
            email,
            balance,
            contact
        } = req.body;
        const newAccount = new Account({
            first_name: first_name,
            last_name: last_name,
            email: email,
            balance: balance,
            contact: contact
        })
        const accountCreated = await newAccount.save();
        res.status(200).json({
            "response": accountCreated
        })
    } catch (error) {
        console.log(error.message)
        res.status(422).json({
            "error": error.message
        });
    }

})

app.post('/accounts/transfer', async (req, res) => {
    const { email_origen, email_destino, amount} = req.body;
    const cuentaOrigen = await Account.findOne({email: email_origen});
    const cuentaDestino = await Account.findOne({email: email_destino});
    const session = await mongoose.startSession();
    try {
    session.startTransaction();
    if (cuentaOrigen && cuentaDestino) {
        if (cuentaOrigen.balance > amount) {
            await Account.updateOne({email: email_destino}, {$set:{
                balance: cuentaDestino.balance + amount
            }});
            await session.commitTransaction();
            session.endSession();
            res.status(200).json({"response": cuentaDestino})
        }
    }
    } catch (error) {
        await session.abortTransaction();
        res.status(422).json({"error": error.message});
    }
})

app.put('/accounts', async (req, res) => {
    try {
        const {
            email,
            amount
        } = req.body;

        const account = await Account.findOne({email: email});
        
        await Account.updateOne({
            email: email
        }, {
            $set: {
                balance: account.balance + amount
            }
        })

        const updateAccount = await Account.findOne({email: email});
        res.status(201).json({
            "response": updateAccount
        });
    } catch (error) {
        res.status(422).json({"error": error.message});
    }
})



app.listen(config.port, console.log(`listening on port ${config.port}`));
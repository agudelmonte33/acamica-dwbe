const mongoose = require('mongoose');
const config = require('./config');
try {
    mongoose.connect(`${config.mongo_db}/${config.colecction}`)
} catch (error) {
    console.log(error.message);
}

module.exports = mongoose;
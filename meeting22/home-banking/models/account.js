const mongoose = require('../connection');


const Account = mongoose.model('accounts', {
    first_name: String,
    last_name: String,
    email: String,
    balance: Number,
    contact: {phone: Number,
        social_network: String,
        address: String}
});

module.exports = Account
require('dotenv').config();

const config = {
    port: process.env.NODE_PORT || 3000,
    mongo_db: process.env.MONGODB ||'mongodb://localhost:27017',
    colecction: process.env.COLECCTION || 'home-banking'
}

module.exports = config
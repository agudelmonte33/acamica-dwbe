let usuariosRegistrados = [];


//EVENTO REGISTRAR USUARIO
document.querySelector('#btn-registrarse').addEventListener('click', function() {

    let nombre = document.getElementById('nombre').value;
    let apellido = document.getElementById('apellido').value;
    let email = document.getElementById('email').value;
    let pais = document.getElementById('pais').value;
    let password = document.getElementById('password').value;
    let passwordRepetida = document.getElementById('password-repetida').value;

    //validar campos vacíos 
    if (nombre === "" || apellido === "" || email === "" || pais === "" || password === "" || passwordRepetida === "") {
        alert("Todos los campos son obligatorios")
        return;
    }

    //validar email
    for (i = 0; i < usuariosRegistrados.length; i++) {
        if (email == usuariosRegistrados[i].email) {
            alert("El email ingresado ya se encuentra registrado")
            return;
        }
    }

    //validar contraseñas
    if (password != passwordRepetida) {
        alert("Las constraseñas deben ser iguales")
        return;
    }

    //instanciar y agregarlo al array usuariosRegistrados
    nuevoUsuario = new Usuario(nombre, apellido, email, pais, password);
    usuariosRegistrados.push(nuevoUsuario);
    alert("Usuario ingresado correctamente");
})

//EVENTO LOGIN
document.querySelector('#btn-login').addEventListener('click', function() {

    const emailLogin = document.getElementById('email-login').value;
    const passwordLogin = document.getElementById('password-login').value;

    //buscar y retornar índice
    let indiceUser = usuariosRegistrados.findIndex(function(elemento) {
        return elemento.email == emailLogin && elemento.pass == passwordLogin;
    });

    //Validar campos 
    if (emailLogin === "" || passwordLogin === "") {
        alert("Todos los campos son obligatorios")
        return;
    }

    //validar si el usuario existe
    if (indiceUser != -1) {
        alert("Bienvenido " + nuevoUsuario.nombre);
        return indiceUser;
    } else {
        alert("El email o contraseña no coinciden");
        return false;
    }
})
const Product = require("../models/product");

async function checkValidProduct(req, res, next) {
    try {
        const idProduct = req.body._id;
    const findProduct = await Product.findById(idProduct);
    if (findProduct) {
        next();
    } else {
        res.status(400).json({
            "response": "El producto no fue encontrado",
        });
    }
    } catch (error) {
        res.status(422).json({
            "response": "El ID del producto no tiene un formato válido",
        });
    }
    

}

module.exports = {
    checkValidProduct,
};
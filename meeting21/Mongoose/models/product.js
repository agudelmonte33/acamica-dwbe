const mongoose = require('../connection');
const Product = mongoose.model('products', {
    product: String,
    price: Number,
    product_type: String
})

module.exports = Product
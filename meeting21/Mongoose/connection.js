const mongoose = require('mongoose');
const config = require('./config');

mongoose.connect(`${config.mongoDB}/Delilah`)

module.exports = mongoose;
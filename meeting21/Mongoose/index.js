const express = require('express');
const app = express();
const config = require('./config');
const products = require('./routes/products')

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use('/products', products);

app.listen(config.port, console.log(`Escuchando en el puerto ${config.port}`))
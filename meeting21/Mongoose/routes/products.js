const express = require('express');
const app = express();
const router = express.Router();
const Product = require('../models/product');
const {
    checkValidProduct
} = require('../middlewares/validProduct')

app.use(express.json());
// app.use(express.urlencoded({
//     extended: true
// }));

//Creat a product
router.post('/', async (req, res) => {
    const productName = req.body.product_name;
    const productPrice = req.body.price;
    const productType = req.body.product_type;
    const newProduct = new Product({
        product: productName,
        price: productPrice,
        product_type: productType
    })
    const product = await newProduct.save();
    res.status(201).json({
        "response": product
    });
});

//Single producto
//TODO: pasar el ID por path
router.get('/', checkValidProduct, async (req, res) => {
    const idProduct = req.body._id;
    const response = await Product.find({
        _id: idProduct
    })
    res.status(200).send(response);
});

//List of products
//TODO: modificar path
router.get('/allProducts', async (req, res) => {
    const response = await Product.find({});
    res.status(200).send(response);
});

//Update a product
//TODO: pasar ID por path
router.put('/', checkValidProduct, async (req, res) => {
    const idProduct = req.body._id;
    const productName = req.body.product_name;
    const productPrice = req.body.price;
    const productType = req.body.product_type;
    const doc = await Product.findOneAndUpdate({
        _id: idProduct
    }, {
        $set: {
            product: productName,
            price: productPrice,
            product_type: productType
        }
    });
    
    const updateDoc = await Product.findById(idProduct);
    
    res.status(200).json({
        "response": updateDoc
    });
});

//Delete a product
//TODO: pasar ID por path
router.delete('/', checkValidProduct, async (req, res) => {
    const idProduct = req.body._id;
    try {
        await Product.deleteOne({
            _id: idProduct
        });
        res.status(200).json({
            "response": "Se eliminó el producto con éxito"
        })
    } catch (error) {
        //Cual sería la forma apropiada de mostrar el error??
        console.log(error);
        res.status(401).json({
            "response": "Hubo un error"
        })
    }

})



module.exports = router
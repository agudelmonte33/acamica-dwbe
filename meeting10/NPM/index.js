const coolImages = require("cool-images"),
    fs = require('fs'),
    moment = require('moment');

let imagenes = coolImages.many(600, 800, 10),
    myDate = new Date(),
    myUrlDate = moment(myDate).format('LLLL');



imagenes.forEach(url => {
    fs.appendFile('./log.txt', `\n${url} - ${myUrlDate}`, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log(`Se registro la url con éxito \n${url} - ${myUrlDate}`)
        }
    })
});
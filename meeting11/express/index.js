'use strict'

const express = require('express');
const app = express();

let alumnos = [
    { nombre: "Carla", edad: 30 },
    { nombre: "Luis", edad: 27 },
    { nombre: "Pablo", edad: 32 },
    { nombre: "Majo", edad: 25 }
];

let nuevoAlumno = { nombre: "Lucas", edad: 21 };



app.post('/alumnos', function(req, res) {
    alumnos.push(nuevoAlumno);
    res.send(alumnos);
});

app.get('/alumnos', function(req, res) {
    res.send(alumnos);
});


app.listen(3000, function() {
    console.log('Escuchando el puerto 3000!');
});
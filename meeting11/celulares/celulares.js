let arrayCelulares = [
    { marca: "Sansumg", gama: "alta", modelo: "s20", pantalla: "5", so: "andriod", precio: "20000" },
    { marca: "Sansumg", gama: "alta", modelo: "s20", pantalla: "5", so: "andriod", precio: "18000" },
    { marca: "Sansumg", gama: "media", modelo: "s20", pantalla: "5", so: "andriod", precio: "15000" },
    { marca: "Sansumg", gama: "media", modelo: "s20", pantalla: "5", so: "andriod", precio: "17000" },
    { marca: "Sansumg", gama: "baja", modelo: "s20", pantalla: "5", so: "andriod", precio: "25000" },
    { marca: "Sansumg", gama: "baja", modelo: "s20", pantalla: "5", so: "andriod", precio: "10000" },
    { marca: "Sansumg", gama: "baja", modelo: "s20", pantalla: "5", so: "andriod", precio: "12000" },
]

module.exports = arrayCelulares
const arrayCelulares = require("./celulares");

function mayorPrecioCelulares(res) {
    arrayCelulares.sort((a, b) => a.precio - b.precio);
    return arrayCelulares[arrayCelulares.length - 1];
}

function menorPrecioCelulares(res) {
    arrayCelulares.sort((a, b) => a.precio - b.precio);
    return arrayCelulares[0];
}

function gamaCelulares() {
    let gamaAlta = [],
        gamaMedia = [],
        gamaBaja = [];
    for (let i = 0; i < arrayCelulares.length; i++) {
        switch (arrayCelulares[i].gama) {
            case 'alta':
                gamaAlta.push(arrayCelulares[i]);
                break;
            case 'media':
                gamaMedia.push(arrayCelulares[i]);
                break;
            case 'baja':
                gamaBaja.push(arrayCelulares[i]);
                break;
            default:
                break;
        }
    }
    return gamas = [gamaAlta, gamaMedia, gamaBaja];
}

function mitadArrayCelulares() {
    const aux = (arrayCelulares.length) / 2;
    const mitadArray = arrayCelulares.slice(0, aux);
    return mitadArray;
}

module.exports = { mayorPrecioCelulares, menorPrecioCelulares, gamaCelulares, mitadArrayCelulares }
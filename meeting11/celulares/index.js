//REQUERIMIENTOS
const config = require('./config');
const express = require('express');
const app = express();

const funciones = require('./funciones');

//ENDPOINTS
app.get('/cel-menor-precio', (req, res) => {
    res.json(funciones.menorPrecioCelulares());
})
app.get('/cel-mayor-precio', (req, res) => {
    res.json(funciones.mayorPrecioCelulares());
})
app.get('/cel-gamas', (req, res) => {
    res.json(funciones.gamaCelulares());
})
app.get('/cel-mitad-array', (req, res) => {
    res.json(funciones.mitadArrayCelulares());
})

app.listen(config.port, console.log(`estamos escuchando el puerto 3000`));
function register(e) {
    const url = "https://delilah-resto.cf/api/users";

    e.preventDefault();

    const form = document.getElementById("form");

    const firstName = form.firstName.value;
    const lastName = form.lastName.value;
    const nickname = form.nickname.value;
    const email = form.email.value;
    const phone = form.phone.value;
    const pass = form.pass.value;

    if (!firstName || !lastName || !nickname || !phone || !pass || !email) {
        alert("Todos los campos son requeridos");
        return false;
    }

    const formData = {
        first_name: firstName,
        last_name: lastName,
        nickname: nickname,
        email: email,
        phone: phone,
        password: pass
    };

    console.log("registrar usuario");

    console.log(formData);

    fetch(url, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            },
        })
        .then((r) => r.json().then((data) => ({
            status: r.status,
            body: data
        })))
        .then((info) => {
            console.log(info);
            if (info.status === 200 || info.status === 201) {
                console.log("status: " + info.status)
                window.location.href = "registerOK.html";
            } else {
                alert(info.body.message)
            }

        
        })
        .catch(error => {
            alert(error)
        })
}
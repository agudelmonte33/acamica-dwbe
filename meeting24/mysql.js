const express = require('express');
const app = express();
const config = require('./config');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.db, config.user, config.pass, {
    host: config.host,
    dialect: 'mariadb'
});

app.use(express.json());

// try {
//     await sequelize.authenticate();
//     console.log('Connection has been established successfully.');
// } catch (error) {
//     console.error('Unable to connect to the database:', error.message);
// }
app.get('/bands', async (req, res) => {
    

    sequelize.query('SELECT * FROM songs', {
        type: sequelize.QueryTypes.SELECT
    })
    .then(result => {
        res.json(result)
    });
})
app.post('/bands', async (req, res) => {
    const { nombre, integrantes, fecha_inicio, fecha_separacion, pais } = req.body;
    const band = await sequelize.query(`INSERT INTO bands (nombre, integrantes, fechas_inicio, fecha_separacion, pais) VALUES ("${nombre}", "${integrantes}", "${fecha_inicio}", "${fecha_separacion}", "${pais}")`, {
        type: sequelize.QueryTypes.INSERT
    })
    res.json({ band });
})
// app.put('/bands', async (req, res) => {

//     sequelize.query('SELECT * FROM songs', {
//         type: sequelize.QueryTypes.SELECT
//     })
//     .then(result => {
//         res.json(result)
//     });
// })
// app.delete('/bands', async (req, res) => {

//     sequelize.query('SELECT * FROM songs', {
//         type: sequelize.QueryTypes.SELECT
//     })
//     .then(result => {
//         res.json(result)
//     });
// })


app.listen(config.node_port, console.log(`Listening in ${config.node_port}`));
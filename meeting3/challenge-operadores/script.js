let operacion1 = document.getElementById('operacion1'),
    operacion2 = document.getElementById('operacion2'),
    operacion3 = document.getElementById('operacion3'),
    operacion4 = document.getElementById('operacion4'),
    operacion5 = document.getElementById('operacion5');
let a = 4,
    b = 5,
    c = 6,
    d = 1,
    resultado;

// Operación 1
resultado = b + a;
console.log(resultado);
operacion1.innerHTML = ` ${resultado}`;

//operación 2
resultado = b + d;
console.log(resultado);
operacion2.innerHTML = ` ${resultado}`;

//operación 3
resultado = resultado + c;
console.log(resultado);
operacion3.innerHTML = ` ${resultado}`;

//operación 4
resultado = b / a;
console.log(resultado);
operacion4.innerHTML = ` ${resultado}`;

//operación 5
resultado = b % a;
console.log(resultado);
operacion5.innerHTML = ` ${resultado}`;
const {user, pass, host, db} = require('../config/config');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(db, user, pass, {
    host: host,
    dialect: 'mariadb'
});

module.exports = sequelize;

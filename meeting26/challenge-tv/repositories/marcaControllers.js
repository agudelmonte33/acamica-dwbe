const sequelize = require('../connection/sequelize');
const {Marca, Modelo} = require('../models/models');


async function postMarca(nombre){
    const marca = await Marca.create({
        nombre: nombre
    });
    return marca.toJSON();
}

async function getModelosByMarca(idMarca) {
    const result = await Marca.findOne({
        where: {
            id: idMarca
        },
        include: {
            model: Modelo,
            as: "modelos"
        }
    })
    return result.toJSON();
}

module.exports = {postMarca, getModelosByMarca};
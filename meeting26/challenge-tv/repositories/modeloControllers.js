const {Op} = require('sequelize');
const {Modelo} = require('../models/models');

async function postModelo(modelo, pantalla, smart, precio, marcaId){
    const nuevoModelo = await Modelo.create({
        modelo: modelo,
        pantalla: pantalla,
        smart: smart,
        precio: precio,
        marcaId: marcaId,
    });
    return nuevoModelo.toJSON();
}

async function getMayorPrecio(monto){
    return await Modelo.findAll({
        where:{
            precio: {
                [Op.gt]: monto
            }
        }
    })
}
async function getMenorPrecio(monto){
    return await Modelo.findAll({
        where:{
            precio: {
                [Op.lt]: monto
            }
        }
    })
    
}

async function getOrderMenorMayorPrecio(monto){
    return await Modelo.findAll({
        order: [['precio', 'ASC']]
    })
    
}

module.exports = {postModelo, getMenorPrecio, getMayorPrecio, getOrderMenorMayorPrecio}
const express = require('express');
const app = express();
const {node_port} = require('./config/config')
const sequelize = require('./connection/sequelize');

( async () => {
    await sequelize.sync();
})();

app.use(express.json());

const marcas = require('./routes/marcas')
app.use('/marcas', marcas);

const modelos = require('./routes/modelos')
app.use('/modelos', modelos);





app.listen(node_port, console.log(`Listening on port: ${node_port}`));


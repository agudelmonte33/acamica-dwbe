const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');

class Marca extends Model {}

Marca.init({
    nombre: DataTypes.STRING
}, {sequelize, modelName: 'marcas'});

class Modelo extends Model {}

Modelo.init({
    modelo: DataTypes.STRING,
    pantalla: DataTypes.INTEGER,
    smart: DataTypes.BOOLEAN,
    precio: DataTypes.INTEGER,
}, {sequelize, modelName: 'modelos'})

Marca.hasMany(Modelo, { as: "modelos"});



module.exports = {Marca, Modelo}

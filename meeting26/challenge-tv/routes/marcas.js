const express = require('express');
const app = express();
const router = express.Router();
const {postMarca,getModelosByMarca} = require('../repositories/marcaControllers');

app.use(express.json());

router.get('/:idMarca/modelos', async (req, res) => {
    const idMarca = req.params.idMarca;
    const response = await getModelosByMarca(idMarca)
    console.log(response);
    res.status(200).json({response})
})
router.post('/', async (req,res) => {
    const nombre = req.body.nombre;
    const response = await postMarca(nombre);
    res.status(201).json({response})
})


module.exports = router
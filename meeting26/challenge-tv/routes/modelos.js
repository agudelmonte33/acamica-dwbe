const express = require('express');
const app = express();
const router = express.Router();
const {postModelo, getMenorPrecio, getMayorPrecio, getOrderMenorMayorPrecio} = require('../repositories/modeloControllers');

app.use(express.json());

router.post('/:marcaId', async (req, res) => {
    const {marcaId} = req.params;
    const {modelo, pantalla, smart, precio} = req.body;
    const response = await postModelo(modelo, pantalla, smart, precio, marcaId);
    res.status(200).json({response})
})

router.get('/mayor_precio/:precio', async (req, res) => {
    const {precio} = req.params;
    const response = await getMayorPrecio(precio);
    res.status(200).json({response});
})
router.get('/menor_precio/:precio', async (req, res) => {
    const {precio} = req.params;
    const response = await getMenorPrecio(precio);
    res.status(200).json({response});
})

router.get('/menor_a_mayor', async (req, res) => {
    const response = await getOrderMenorMayorPrecio();
    res.status(200).json({response});
})


module.exports = router;
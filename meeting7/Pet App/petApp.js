//Divido los array de esta forma para poder filtrarlos 
let totalPerros = [],
    enAdopcion = [],
    adoptados = [],
    enProcesoDeAdopcion = [];

const EN_ADOPCION = "EN ADOPCIÓN",
    ADOPTADO = "ADOPTADO",
    EN_PROCESO_DE_ADOPCION = "EN PROCESO DE ADOPCIÓN";
class Perros {
    constructor(nombre, sexo, edad) {
        this._nombre = nombre,
            this._sexo = sexo,
            this._edad = parseInt(edad),
            this._estado = EN_ADOPCION
    }
    modificarEstado(value) {
        this._estado = value;
    }
    informarEstado() {
        return `Este perro esta ${this._estado}`;
    }
}

//perros de ejemplo
let perro1 = new Perros("Chicho", "macho", "3"),
    perro2 = new Perros("Cumbia", "hembra", "2"),
    perro3 = new Perros("Laica", "hembra", "5"),
    perro4 = new Perros("Boby", "macho", "3"),
    perro5 = new Perros("candy", "hembra", "2"),
    perro6 = new Perros("Luna", "hembra", "5");

//Modifico algunos estados para tener variedad en el array inicial
perro2.modificarEstado(ADOPTADO);
perro3.modificarEstado(EN_PROCESO_DE_ADOPCION);
perro5.modificarEstado(ADOPTADO);
perro6.modificarEstado(EN_PROCESO_DE_ADOPCION);

//Los agrego al array totalPerros
totalPerros.push(perro1, perro2, perro3, perro4, perro5, perro6);


//Imprimero el totalPerros para tener la lista en pantalla al principio
imprimirListaPerros(totalPerros);


//FUNCIONES

//Imprime la lista en HTML
function imprimirListaPerros(listaPerros) {
    let lista = "";
    for (let i = 0; i < listaPerros.length; i++) {
        lista += `<li><strong>Nombre:</strong> ${listaPerros[i]._nombre}, <strong>sexo:</strong> ${listaPerros[i]._sexo}, <strong>edad:</strong> ${listaPerros[i]._edad} años, <strong>estado:</strong> ${listaPerros[i]._estado}</li>`;
    }
    document.querySelector('#lista-perros').innerHTML = lista;
}


//FILTROS

//En adopción
function filtrarEnAdopcion() {
    enAdopcion = totalPerros.filter(perro => perro._estado === EN_ADOPCION);
};
//En proceso de adopción
function filtrarEnProcesoDeAdopcion() {
    enProcesoDeAdopcion = totalPerros.filter(perro => perro._estado === EN_PROCESO_DE_ADOPCION);
};
//Adoptados
function filtrarAdoptados() {
    adoptados = totalPerros.filter(perro => perro._estado === ADOPTADO);
};

//EVENTO INGRESAR NUEVO PERRO
document.querySelector('#cargar').addEventListener('click', function() {
    do {
        let nombre = prompt("Ingrese el nombre del perro"),
            sexo = prompt("Ingrese el sexo"),
            edad = prompt("Ingrese la edad");
        let nuevoPerro = new Perros(nombre, sexo, edad);
        totalPerros.push(nuevoPerro);
    } while (window.confirm("¿Desea agregar otro perro?"))
    imprimirListaPerros(totalPerros);
})


//EVENTO FILTRAR EN ADOPCION
document.querySelector('#en-adopcion').addEventListener('click', function() {
    filtrarEnAdopcion();
    imprimirListaPerros(enAdopcion);
})

//EVENTO FILTRAR ADOPTADOS
document.querySelector('#adoptados').addEventListener('click', function() {
    filtrarAdoptados();
    imprimirListaPerros(adoptados);
})

//EVENTO FILTRAR EN PROCESOS DE ADOPCION
document.querySelector('#en-proceso').addEventListener('click', function() {
    filtrarEnProcesoDeAdopcion();
    imprimirListaPerros(enProcesoDeAdopcion);
})

//EVENTO FILTRAR TODOS
document.querySelector('#todos').addEventListener('click', function() {
    imprimirListaPerros(totalPerros);
})
class Estudiante {
    constructor(nombre, apellido, edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    presentacion() {
        return `Hola! mi nombre es ${this.nombre} ${this.apellido} y tengo ${this.edad} años`;
    }

    asistirAClase() {
        return "Estoy presente en la clase";
    }

    preguntar(pregunta) {
        return `tengo una duda, ${pregunta}`;
    }

    resolverChallenge() {
        return "a trabajar!";
    }
}

let estudiante1 = new Estudiante("Agustin", "Villagran", 29);
let estudiante2 = new Estudiante("Daniel", "Rueda", 25);
let estudiante3 = new Estudiante("Laura", "García", 27);



console.log(estudiante1);
console.log(estudiante2);
console.log(estudiante3);
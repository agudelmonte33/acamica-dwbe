const {add} = require('../add');
const {fibonacci} = require('../fibonacci');
const assert = require('assert');
const expect = require('chai').expect;

describe('test de suma', function() {
    describe('#add.js', function() {
        it('Debería devolver 0 cuando los 2 números sean 0',  function(){
            assert.equal(add(0, 0), 0)
        })

        it('Debería devolver el valor 10 cuando se le pase 4 y 6', function(){
            assert.equal(add(4, 6), 10)
        })

        it('Debería devolver el valor -2 cuando se le pase -1 y -1', function(){
            //arrange
            const a = -1;
            const b = -1;
            //act
            const result = add(a, b);
            //asser
            expect(result).to.equal(-2);
        })
    })
})


describe('Test de fibonacci', function() {
    describe('#fibonacci.js', function() {
        it('Debería devolver el valor 13 cuando se le pase 6', function(){
            //arrange
            const n = 6;
            //act
            const result = fibonacci(n);
            //asser
            expect(result).to.equal(13);
        })

        it('Deberia devolver 1 cuando el valor que se le pase es 0', function(){
            //arrange
            const n = 0;
            //act
            const result = fibonacci(n);
            //asser
            expect(result).to.equal(1);
        })
    })
})
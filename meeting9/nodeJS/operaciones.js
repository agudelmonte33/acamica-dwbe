function sumar(a, b) {
    return (a + b);
}

function restar(a, b) {
    return (a - b);
}

function mult(a, b) {
    return (a * b);
}

function div(a, b) {
    return (a / b);
}

module.exports = { sumar, restar, mult, div };
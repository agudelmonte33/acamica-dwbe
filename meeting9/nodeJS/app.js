require('dotenv').config();
const operaciones = require('./operaciones');
const chalk = require('chalk');
const fs = require('fs');
const BD = process.env.DB_HOST;

let a = 47,
    b = 35;

let resultado1 = operaciones.sumar(a, b),
    resultado2 = operaciones.restar(a, b),
    resultado3 = operaciones.mult(a, b);

fs.appendFile('./log.txt', `\n${a} + ${b} = ${resultado1}`, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log(chalk.bgRed.bold("se registro el resultado con éxito"));
    }
})

fs.appendFile('./log.txt', `\n${a} - ${b} = ${resultado2}`, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log(chalk.bgRed.italic("se registro el resultado con éxito"));
    }
})

fs.appendFile('./log.txt', `\n${a} * ${b} = ${resultado3}`, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log(chalk.bgRed.inverse("se registro el resultado con éxito"));
    }
})

console.log(BD);
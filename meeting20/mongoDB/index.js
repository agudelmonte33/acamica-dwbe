const express = require('express');
const app = express(); 
const config = require('./config');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/mi_base');

schema = {nombre: String, apellido: String, edad: Number};

const Usuarios = mongoose.model("Usuarios", schema);

const yo = {nombre: "Juan", apellido: "Perez", edad: 24};
let nuevo_usuario = new Usuarios(yo)
nuevo_usuario.save();

Usuarios.find({nombre: "juan"}).then(function (resultados){
    console.log(resultados);
    });


    app.listen(config.port, console.log(`Escuchando en el puerto ${config.port}`))
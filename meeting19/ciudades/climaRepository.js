const axios = require('axios');

async function ciudadClima(ciudad) {
    const respuestaClima = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${ciudad}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
    const datos = respuestaClima.data.main.temp;
    return datos;  
}

function randomCityName(items) {
    return items[Math.floor((Math.random()*items.length))];
}

function arrayTresCiudades(citys) {
    let newArrayCitys = [];
    const eliminarRepetidos = arr => [...new Set(arr)];
    while (newArrayCitys.length < 3) {
        let randomCity = randomCityName(citys);
        newArrayCitys.push(randomCity);
        newArrayCitys = eliminarRepetidos(newArrayCitys);
    };
    return newArrayCitys
};

async function asyncIteratorCitys(array) {
    for(let city of array){
        city.temperatura = await ciudadClima(city.name);
    }
    return array;
}


module.exports = {ciudadClima, arrayTresCiudades, asyncIteratorCitys}
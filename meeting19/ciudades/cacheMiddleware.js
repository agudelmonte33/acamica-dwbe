const {arrayTresCiudades, ciudadClima} = require('./climaRepository')
const client = require('./config');
const citys = require('./data');

//Cache middleware
function cache(req, res, next) {
    const {cityname} = req.params;
    client.get(cityname, (err, data) => {
        if (err) throw err;
        req.dataCache = JSON.parse(data);
        next();
    })
}

function cacheRandomCitys(req, res, next) {
    let arrayCitys = arrayTresCiudades(citys)
    client.get("arrayCitys", (err, data) => {
        if (err) throw err;
        req.dataCache = JSON.parse(data);
        next();
    })
}

module.exports = {cache, cacheRandomCitys}
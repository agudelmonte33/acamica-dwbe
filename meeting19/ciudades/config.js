const { createClient } = require('redis');
const redis_port = 6379;
const client = createClient(redis_port);

module.exports = client
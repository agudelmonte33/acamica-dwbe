const express = require("express");
const app = express();
const client = require('./config')
const {cache, cacheRandomCitys} = require('./cacheMiddleware');
const {ciudadClima, arrayTresCiudades, asyncIteratorCitys} = require('./climaRepository');
const citys = require('./data');

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));


app.get('/climasNuevo/:cityname', cache, async (req, res) => {  
    const cache = req.dataCache;
    console.log(cache);
    if (cache != null) {
        res.status(200).json({"temperatura": cache});
    } else {
        const {cityname} = req.params;
        const response = await ciudadClima(cityname);
        client.set(cityname, response, 'EX', 30);
        res.status(200).json({"temperatura": response});
    }
});

app.get('/climasNuevo', cacheRandomCitys, async (req, res) => {  
    const cache = req.dataCache;
    if (cache != null) {
        res.status(200).json({"temperatura": cache});
    } else {
        let arrayCitys = arrayTresCiudades(citys);
        arrayCitys = await asyncIteratorCitys(arrayCitys);
        client.set("arrayCitys", JSON.stringify(arrayCitys), 'EX', 10);
        res.status(200).json({"temperatura": arrayCitys});
    }

});






client.on("error", function(error){
    console.error(error);
});

app.listen(3000, console.log("Escuchando en el puerto 3000"))

import fetch from 'node-fetch';
import express from 'express';
const app = express()
const port = 3000
import { createClient } from 'redis';
const redis_port = 6379;


const client = createClient(redis_port);

//set response
function setResponse(username, repos) {
    return `<h2>${username} tiene ${repos} repositorios</h2>`
}

//make request to github for data
async function getRepos(req, res) {
    try {
        console.log('Fetching data...');
        const {username} = req.params
        const response = await fetch(`https://api.github.com/users/${username}`);
        const data = await response.json();
        const repos = data.public_repos;
        //set to redis
        client.setex(username, 3600, repos);
        res.send(setResponse(username, repos));
    } catch (error) {
        console.error(error);
        res.status(500);
    }
}

//Cache middleware
function cache(req, res, next) {
    const {username} = req.params;
    client.get(username, (err, data) => {
        if (err) throw err;
        if (data !== null) {
            res.send(setResponse(username, data))
        } else {
            next();
        }
    })
}

app.get('/repos/:username', cache, getRepos);



client.on("error", function(error){
    console.error(error);
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});
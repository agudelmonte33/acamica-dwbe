function validarIdAutor(req, res, next) {
    const idAutor = parseInt(req.params.id);

    if (!Number.isInteger(idAutor)) {
        res.status(422).json({ msg: "El id debe ser un número entero" })
        return
    } else {
        next()
    }

}

function validarIdLibro(req, res, next) {
    const idLibro = parseInt(req.params.id);

    if (!Number.isInteger(idLibro)) {
        res.status(422).json({ msg: "El id debe ser un número entero" })
        return
    } else {
        next()
    }
}

module.exports = { validarIdAutor, validarIdLibro }
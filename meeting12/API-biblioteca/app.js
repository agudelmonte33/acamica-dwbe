const express = require('express');
const app = express();
const autores = require('./data');
const config = require('./config');
const middles = require('./middlewares');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Acamica API',
            version: '1.0.0'
        }
    },
    apis: ['./app.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

/**
 * @swagger
 * /autores:
 *  post:
 *    description: Crea un nuevo autor
 *    parameters:
 *    - name: nombre
 *      description: Nombre del autor
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: apellido del autor
 *      in: formData
 *      required: true
 *      type: string
 *    - name: fechaNacimiento
 *      description: fecha de nacimiento del autor
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */


//ENDPOINT

//acciones
app.get('/autores', (req, res) => {
    res.json(autores);
});

app.post('/autores', (req, res) => {
    res.status(201).json("se ha agregado un autor con éxito");
});

app.get('/autores/:id', middles.validarIdAutor, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);

    //valida la existencia del autor
    if (autor) {
        res.status(200).json(autor);
    } else {
        res.status(400).json(`No se encuentra el autor con el id ${idAutor}`);
    }
})

app.delete('/autores/:id', middles.validarIdAutor, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);

    //valida la existencia del autor
    if (!autor) {
        res.status(400).json(`No se encuentra el autor con el id ${idAutor}`);
        return
    }

    //obtiene el índice del autor en el array
    const indexAutor = autores.indexOf(autor);

    //elimina el autor
    autores.splice(indexAutor, 1);

    res.json(autores);
})

app.put('/autores/:id', middles.validarIdAutor, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);

    //valida la existencia del autor
    if (autor) {
        res.status(200).json(`Se ha modificado el autor con id ${idAutor}`);
    } else {
        res.status(400).json(`No se encuentra el autor con el id ${idAutor}`);
    }

})


//libros
app.get('/autores/:id/libros', middles.validarIdAutor, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);

    //valida si el autor existe
    if (!autor) {
        res.status(400).json(`No se encuentran los libros del autor con el id ${idAutor}`);
    }

    //obtiene el índice del autor en el array
    const indexAutor = autores.indexOf(autor);

    //respuesta con los libros del autor
    res.status(200).json(autores[indexAutor].libros);
})

app.post('/autores/:id/libros', middles.validarIdAutor, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);
    res.status(201).json(`se ha agregado un libro al autor con id ${idAutor}`);
})

app.get('/autores/:id/libros/:idLibro', middles.validarIdLibro, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);

    //valida si el autor existe
    if (!autor) {
        res.status(400).json(`No se encuentran el autor con el id ${idAutor}`);
    }

    //obtiene el id del libro de la ruta
    const idLibro = parseInt(req.params.idLibro);
    const libro = autor.libros.find(libro => libro.id === idLibro);

    //valida la existencia del libro dentro del autor
    if (!libro) {
        res.status(400).json(`No se encuentran el libro con el id ${idLibro}`);
    }

    res.status(200).json(libro);

})

app.put('/autores/:id/libros/:idLibro', middles.validarIdLibro, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);

    //valida la existencia del autor
    if (!autor) {
        res.status(400).json(`No se encuentran el autor con el id ${idAutor}`);
    }

    //obtiene el id del libro de la ruta
    const idLibro = parseInt(req.params.idLibro);
    const libro = autor.libros.find(libro => libro.id === idLibro);

    //valida la existencia del libro dentro del autor
    if (!libro) {
        res.status(400).json(`No se encuentran el libro con el id ${idLibro}`);
    }

    res.status(201).json(`Se ha modificado el libro con id ${idLibro} del autor con id ${idAutor}`);
})

app.delete('/autores/:id/libros/:idLibro', middles.validarIdAutor, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = autores.find(autor => autor.id === idAutor);

    //valida la existencia del libro dentro del array
    if (!autor) {
        res.status(400).json(`No se encuentran el autor con el id ${idAutor}`);
    }

    //obtiene el id del libro de la ruta
    const idLibro = parseInt(req.params.idLibro);
    const libro = autor.libros.find(libro => libro.id === idLibro);
    const librosAutor = autor.libros;

    //valida la existencia del libro dentro del autor
    if (!libro) {
        res.status(400).json(`No se encuentran el libro con el id ${idLibro}`);
    }

    //obtiene el índice del libro dentro del autor
    const indexLibro = autor.libros.indexOf(libro);

    //elimina el libro del autor
    autor.libros.splice(indexLibro, 1)

    res.status(201).json(`Se ha eliminado el libro con id ${idLibro} del autor con id ${idAutor}`)

})

app.listen(config.port, console.log(`estamos escuchando el puerto 3000`));
const autores = [{
    id: 1,
    nombre: "Jorge Luis",
    apellido: "Borges",
    fechaDeNacimiento: "24/08/1899",
    libros: [{
            id: 1,
            titulo: "Ficciones",
            descripcion: "cuentos",
            anioPublicacion: 1944
        },
        {
            id: 2,
            titulo: "El Aleph",
            descripcion: "otro libro de cuentos",
            anioPublicacion: 1949
        }
    ]
}]

module.exports = autores
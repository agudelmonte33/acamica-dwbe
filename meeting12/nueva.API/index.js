const config = require('./config');
const express = require('express');
const app = express();
const fs = require('fs');

//array de cursos disponibles
let cursos = [
    { id: 1, nombre: "desarrollo fullstack" },
    { id: 2, nombre: "desarrollo frontend" },
    { id: 3, nombre: "desarrollo backend" }
];


const log = (req, res, next) => {
    fs.appendFile('./log.txt', `Se registró el método ${req.method} en el endpoint ${req.path}\n`, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log(`Se registró el método ${req.method} en el endpoint ${req.path}\n`);
        }
        next();
    })
}

app.use(log);

app.get('/cursos', (req, res) => {
    res.send(cursos);
});



app.listen(config.port, () => console.log("listening on 3000"));